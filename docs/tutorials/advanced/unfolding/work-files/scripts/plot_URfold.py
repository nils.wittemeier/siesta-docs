#! /usr/bin/env python3
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm, Normalize
import numpy as np
from scipy.interpolate import griddata, RectBivariateSpline

def parse_args():
    # parse command line arguments
    import argparse

    parser = argparse.ArgumentParser(
            description="Plot refolded or unfolded bands created from Util/Unfolding.")

    parser.add_argument("bandsfile", type=str,
                        help="file with re-/unfolded bands")

    parser.add_argument("-r", "--reffile", type=str,
                        help="file with re-/unfolded bands used as a reference",
                        default=None)

    parser.add_argument("-o", "--outfile", metavar="outfile", type=str,
                        help="output file", default="")

    parser.add_argument("-x", "--xrange", type=float, nargs=2,
                        help="range on x-axis; format \"min:max\"")

    parser.add_argument("-y", "--yrange", type=float, nargs=2,
                        help="range on y-axis; format \"min:max\"")

    parser.add_argument("-z", "--zrange", type=float, nargs=2,
                        help="range on z-axis/colorbar; format \"min:max\"")

    parser.add_argument("--title", type=str, help="plot tile", default='')

    parser.add_argument("--xlabel", type=str, help="label for x-axis",
                        default='')

    parser.add_argument("--ylabel", type=str, help="label for y-axis",
                        default='Energy (eV)')

    parser.add_argument("--zlabel", type=str, help="label for z-axis/colorbar",
                        default=r'qLDOS(eV$^{-1}$)')

    parser.add_argument("--type",
                        choices=["scatter", "imshow"],
                        default="imshow",
                        help="matplotlib routine use to visualize data")

    parser.add_argument("--log-color", action='store_true',
                        help="enable logarithmic scale color norm")

    parser.add_argument("--log-size", action='store_true',
                        help="enable logarithmic scale point size (only for scatter plots)")

    parser.add_argument("--no-Ef-shift", action='store_true',
                        help="do not shift Fermi-level to 0 eV")

    parser.add_argument("-k", "--kpoints", type=int, nargs='+',
            help="indices of high-symmetry points (zero-based)")

    parser.add_argument("-l", "--klabels", type=str, nargs='+',
            help="labels of high-symmetry points")

    parser.add_argument("-s", "--figsize", type=float, nargs=2,
            help="width and height of the figure (in inches)",
            default=[6.4, 4.8])

    parser.add_argument("--dpi", type=int,
            help="resultion of the figure",
            default=100)

    parser.add_argument("--fontsize", type=int,
            help="resultion of the figure",
            default=None)

    parser.add_argument("--cmap", type=str,
            help="color map used in figure (refer to https://matplotlib.org/tutorials/colors/colormaps.html)",
            default='viridis')


    return parser.parse_args()

def read_UandRBands(filename):
    with open(filename, 'r') as fp:
        lines=fp.readlines()

    tmp = lines.pop(0).split()
    nq = int(tmp[0])
    ne = int(tmp[1])
    emin = float(tmp[2])
    emax = float(tmp[3])
    fermi = float(tmp[4])

    data = np.zeros((ne,nq))
    qvec = np.zeros((nq, 3))
    iline = 0
    for iq in range(nq):
            qvec[iq] = np.array(lines[iline].split()[0:3])
            iline += 1
            for ie in range(ne):
                    data[ie,iq] = float(lines[iline])
                    iline += 1
    data = np.flip(data, 0)

    return data, qvec, emin, emax, fermi

if __name__ == '__main__':

    args = parse_args()
    save = args.outfile != ""
    if save:
        # matplotlib.use('pdf')
        # plt.rcParams['text.usetex'] = True
        # plt.rcParams['text.latex.preamble'] = [r'\usepackage{lmodern}']
        # plt.rcParams['font.family'] = 'serif'
        # plt.rcParams['font.serif'] = ['Computer Modern']
        pass
        
    if args.fontsize is not None:
        plt.rcParams['font.size'] = args.fontsize

    data, qvec, emin, emax, fermi = read_UandRBands(args.bandsfile)

    if args.reffile is not None:
        r_data, r_qvec, r_emin, r_emax, r_fermi = read_UandRBands(args.reffile)
        assert np.allclose(qvec, r_qvec), 'Q vectors in reference file does not match' 
        assert (emin, emax) == (r_emin, r_emax), f'Energy range in refernce file does not match: {emin}:{emax} ({r_emin}:{r_emax})'
        assert data.shape == data.shape, 'Shape of bands is different, number of k-points and energy points has to be equal' 
        data -= r_data

    if not args.no_Ef_shift:
        emin = emin - fermi
        emax = emax - fermi

    lq = np.linalg.norm(np.roll(qvec, 1, 0) - qvec, axis=1)
    lq[0] = 0
    lq = np.cumsum(lq)
    nq = lq.shape[0]
    qmin = np.min(lq)
    qmax = np.max(lq)
    
    ne = data.shape[0]
    egrid = np.linspace(emin,emax,ne)

    print("Number of energy points: {:d}".format(ne))
    print("Number of q points:      {:d}".format(nq))


    fig, axis = plt.subplots(1,1, dpi=args.dpi, figsize=args.figsize)

    cnorm = LogNorm if args.log_color else Normalize 
    cnorm = cnorm(*args.zrange) if args.zrange is not None else cnorm()

    snorm = LogNorm if args.log_size else Normalize 
    snorm = snorm(*args.zrange) if args.zrange is not None else snorm()

    y, x = np.broadcast_arrays(egrid.reshape((-1, 1)), lq.reshape((1, -1)))

    if args.type == "scatter":
        line = plt.scatter(x.ravel(), np.flip(y.ravel()), s=4*snorm(data.ravel()), c=data.ravel(),
                           cmap=args.cmap, norm=cnorm)

    elif args.type == "imshow":
        # Creating a regular grid and extrapolate data points
        y, x = np.broadcast_arrays(egrid.reshape((-1, 1)), lq.reshape((1, -1)))
        xi = np.linspace(np.min(lq), np.max(lq), 2*nq)
        yi = np.linspace(emin, emax, 2*ne)
        datai = griddata((y.ravel(), x.ravel()), data.ravel(),
                         (yi[:,None], xi[None,:]),
                         method='linear')

        line = axis.imshow(datai, aspect='auto', cmap=args.cmap, 
                              extent= (min(lq), max(lq), emin, emax), norm=cnorm)

    # -- Axis Ranges
    if args.xrange is None:
        args.xrange = (min(lq), max(lq))
    axis.set_xlim(*args.xrange)

    if args.yrange is None:
        args.yrange = (emin, emax)
    axis.set_ylim(*args.yrange) 

    print("Xrange: {: 15.6f}  {: 15.6f}".format(*args.xrange))
    print("Yrange: {: 15.6f}  {: 15.6f}".format(*args.yrange))
    if args.zrange is not None:
        print("Zrange:     {: 15.6e}  {: 15.6e}".format(*args.zrange))

    # -- Labels and key
    axis.set_xlabel(args.xlabel)
    axis.set_ylabel(args.ylabel)
    cbar = fig.colorbar(line, ax=axis)
    cbar.set_label(args.zlabel)


    # -- Vertical line in bands plot
    if args.kpoints is not None:
        axis.xaxis.set_ticks(lq[args.kpoints])
        if args.klabels is not None:
            axis.set_xticklabels(args.klabels)
        for tick in lq[args.kpoints]:
            axis.plot([tick, tick], args.yrange, 'k', linewidth=0.5)

    # -- Display or show figure
    if save:
        plt.savefig(args.outfile)
    else:
        plt.show()
