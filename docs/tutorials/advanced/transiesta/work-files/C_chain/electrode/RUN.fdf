# Prefix for all output files
SystemLabel     electrode

# Include a file with the geometry.
%include elec_geom.fdf

# ---------------------------------------------
#      END OF STRUCTURAL INFORMATION
# ---------------------------------------------

# We use the default Single Zeta Polarized basis.
PAO.BasisSize SZP

# PBE functional
XC.functional      GGA
XC.authors         PBE

# Brillouin zone to use for the calculation. On the transversal
# directions we don't need k points because it is a 1D chain.
# In the direction that we want to make semi infinite, we need
# a huge quantity of k points to make sure it is well sampled.
# Note that the electrode calculation is only done once and then
# you can use it for any system that contains this electrode.
%block kgrid_Monkhorst_Pack
 90  0  0  0.0
  0  1  0  0.0
  0  0  1  0.0 
%endblock kgrid_Monkhorst_Pack

# Since we have many K points and the system is small, we parallelize 
# over k points instead of over orbitals.
Diag.ParallelOverK   true

# Fineness of the real space grid. Again, you can set this to a high
# number to make sure it is converged (you only run the calculation once)
MeshCutoff         400.0 Ry

# Some parameters to control the behavior of the SCF loop.
SCF.Mix              hamiltonian
MaxSCFIterations     100
DM.MixingWeight      0.01
DM.NumberPulay       8
DM.Tolerance         1.d-4

# Store the density matrix in a .TSDE file and the Hamiltonian in 
# a .TSHS file. These two lines are equivalent to passing the --electrode
# flag to the siesta executable. The .TSHS file of the electrode is required
# by the TranSIESTA calculation.
TS.DE.Save             .True.
TS.HS.Save             .True.
